package pl.lukaszkolacz.favoriteplaces.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Collections;

import butterknife.ButterKnife;
import io.realm.Realm;
import pl.lukaszkolacz.favoriteplaces.R;
import pl.lukaszkolacz.favoriteplaces.adapters.PlacesAdapter;

/**
 * Created by Lukasz Kolacz on 22.08.2017.
 */

public class ListFragment extends Fragment {
    private static final String ARG_SECTION_NUMBER = "section_number";
    RecyclerView itemView;
    PlacesAdapter placesAdapter;

    public ListFragment() {
    }

    public static ListFragment newInstance(int sectionNumber) {
        ListFragment fragment = new ListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.list_fragment, container, false);
        ButterKnife.bind(this, rootView);
        itemView = rootView.findViewById(R.id.itemView);

        ItemTouchHelper itemTouchHelper = new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP | ItemTouchHelper.DOWN, 0) {
            @Override
            public boolean onMove(RecyclerView recyclerView, final RecyclerView.ViewHolder viewHolder, final RecyclerView.ViewHolder target) {
                placesAdapter.notifyItemMoved(viewHolder.getAdapterPosition(), target.getAdapterPosition());
                Realm realm = Realm.getDefaultInstance();
                realm.beginTransaction();
                placesAdapter.mPlaces.get(viewHolder.getAdapterPosition()).position = target.getAdapterPosition();
                placesAdapter.mPlaces.get(target.getAdapterPosition()).position = viewHolder.getAdapterPosition();
                realm.commitTransaction();
                Collections.swap(placesAdapter.mPlaces, viewHolder.getAdapterPosition(), target.getAdapterPosition());
                return true;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

            }
        });
        itemTouchHelper.attachToRecyclerView(itemView);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        itemView.setLayoutManager(linearLayoutManager);
        itemView.setHasFixedSize(true);
        placesAdapter = new PlacesAdapter();
        itemView.setAdapter(placesAdapter);
    }
}
