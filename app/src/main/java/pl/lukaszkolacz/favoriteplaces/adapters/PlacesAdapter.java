package pl.lukaszkolacz.favoriteplaces.adapters;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import pl.lukaszkolacz.favoriteplaces.Place;
import pl.lukaszkolacz.favoriteplaces.R;

/**
 * Created by Lukasz Kolacz on 22.08.2017.
 */

public class PlacesAdapter extends RecyclerView.Adapter<PlacesAdapter.ViewHolder> {
    private Realm realm;
    public List<Place> mPlaces = new ArrayList<>();

    public PlacesAdapter() {
        realm = Realm.getDefaultInstance();
        getItems();
    }



    private void getItems() {
        mPlaces.clear();
        RealmResults<Place> places = realm.where(Place.class).findAllSorted("position");
        if (places != null && places.size() > 0) {
            for (Place place : places) {
                mPlaces.add(place);
            }
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View inflate = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_place, parent, false);
        return new ViewHolder(inflate);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        Place place = mPlaces.get(position);
            holder.textViewTitle.setText(place.name);
            holder.textViewNote.setText(place.note);

            holder.deleteButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Place place2delete = mPlaces.get(holder.getAdapterPosition());
                    realm.beginTransaction();
                    place2delete.deleteFromRealm();
                    realm.commitTransaction();

                    mPlaces.remove(holder.getAdapterPosition());
                    notifyItemRemoved(holder.getAdapterPosition());
                }
            });

    }

    @Override
    public int getItemCount() {
        return mPlaces.size();
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView textViewTitle;
        TextView textViewNote;
        FloatingActionButton deleteButton;


        ViewHolder(View itemView) {
            super(itemView);
            textViewTitle = itemView.findViewById(R.id.title);
            textViewNote = itemView.findViewById(R.id.note);
            deleteButton = itemView.findViewById(R.id.deleteButton);
        }
    }
}
